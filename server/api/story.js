var story = function (app) {
    var container = [{
          id: 1,
          name: 'First story',
          description: 'It\'s just a test',
          points: 3
      }, {
          id: 2,
          name: 'Second story',
          description: 'Hi!',
          points: 1
      }]

      app.get('/api/story', function (req, res) {
          res.json(container);
      });

      app.get('/api/story/1', function (req, res) {
          res.json(container[0]);
      });

      app.get('/api/story/2', function (req, res) {
          res.json(container[1]);
      });
}

module.exports = story
