var express = require('express'),
    http = require('http'),
    path = require('path'),
    hbs = require('express3-handlebars'),
    app = express();

app.configure(function(){
    app.set('port', process.env.PORT || 3000);

    app.engine('handlebars', hbs({
        defaultLayout: 'default',
        layoutsDir: path.join(__dirname, './views/layout')
    }));

    app.set('view engine', 'handlebars');
    app.set('views', path.join(__dirname, './views'));
});

// set logging
app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  next();
});

app.get('/', function(req, res){
    res.sendfile(path.join(__dirname, '../app/index.html'));
});

// api endpoinds
require('./api/story')(app);

// mount
app.use(express.static( path.join( __dirname, '../app') ));
app.use(express.static( path.join( __dirname, '../.tmp') ));

// start server
http.createServer(app).listen(app.get('port'), function(){
    console.log('Express App started!');
});
