# global require
'use strict'

require.config

  deps: ['Marionette', 'Bootstrap', 'start']

  shim:
    underscore:
      exports: '_'
    Backbone:
      deps: ['underscore', 'jQuery']
      exports: 'Backbone'
    Marionette:
      deps: ['Backbone']
      exports: 'Backbone'
    Bootstrap:
      deps: ['jQuery']
      exports: 'jQuery'
    Handlebars:
      deps: ['handlebars']
      exports: 'Handlebars'
    routefilter:
      deps: ['Backbone']
      exports: 'Backbone'
  paths:
    jQuery: '../bower_components/jquery/jquery'
    Backbone: '../bower_components/backbone/backbone'
    Marionette: '../bower_components/backbone.marionette/lib/backbone.marionette'
    underscore: '../bower_components/underscore/underscore'
    Bootstrap: 'vendor/bootstrap'

    # Backbone plugins
    routefilter: '../bower_components/backbone.routefilter/src/backbone.routefilter'

    # Handlebars
    handlebars: '../bower_components/require-handlebars-plugin/Handlebars'
    hbs: '../bower_components/require-handlebars-plugin/hbs'
    json2: '../bower_components/require-handlebars-plugin/hbs/json2'
    i18nprecompile: '../bower_components/require-handlebars-plugin/hbs/i18nprecompile'
  hbs:
    disableI18n: true
