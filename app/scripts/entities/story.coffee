define ['Marionette', 'app'], (Backbone, App) ->
  'use strict'

  class App.Entities.Story extends Backbone.Model
    urlRoot: "http://localhost:5000/api/stories"
    defaults:
      name: ""
      description: ""
      points: null
    validate: (attrs, options) ->
      if _.isEmpty attrs.name
        return { name: "name can't be in blank" }

  class App.Entities.StoryCollection extends Backbone.Collection
    url: "http://localhost:5000/api/stories"
    model: App.Entities.Story
    parse: (response) ->
      response.items

  API =
    getStoryEntity: (storyId) ->
      contact = new App.Entities.Story { id: storyId }
      defer = $.Deferred()
      contact.fetch
        success: (data) -> defer.resolve data
        error: (data) -> defer.resolve undefined
      defer.promise()
    getStoryEntities: ->
      contacts = new App.Entities.StoryCollection
      defer = $.Deferred()
      contacts.fetch
        success: (data) -> defer.resolve data
      defer.promise()

  App.Entities.reqres.setHandler "story:entity", (id) ->
    API.getStoryEntity id

  App.Entities.reqres.setHandler "story:entities", ->
    API.getStoryEntities()

  API
