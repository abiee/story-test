define (require) ->
  'use strict'

  Backbone = require 'Marionette'

  class ToolbarView extends Backbone.Marionette.ItemView
    template: ->
      template = require "hbs!apps/story/templates/toolbar"
      template()

    triggers:
      "click #new-story": "story:new"
      "click #stories-list": "story:list"

  class Controller extends Backbone.Marionette.Controller
    initialize: (options) ->
      @region = options.region
    show: ->
      toolbarView = new ToolbarView
      @listenTo toolbarView, "story:new", @_newStory
      @listenTo toolbarView, "story:list", @_storyList
      @region.show toolbarView
    _newStory: -> @trigger "story:new"
    _storyList: -> @trigger "story:list"
