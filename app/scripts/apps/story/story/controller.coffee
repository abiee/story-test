define ['Marionette', 'apps/story/story/story'], (Backbone, StoryView) ->
  'use strict'

  class Controller extends Backbone.Marionette.Controller
    initialize: (options) ->
      @region = options.region
      @story = options.story

    show: ->
      @region.show new StoryView { model: @story }
