define (require) ->
  'use strict'

  Backbone = require "Marionette"

  class StoryView extends Backbone.Marionette.ItemView
    template: (serializedModel) ->
      template = require "hbs!apps/story/templates/story"
      template serializedModel
