define (require) ->
  'use strict'

  Backbone = require 'Marionette'

  class Layout extends Backbone.Marionette.Layout
    template: ->
      template = require "hbs!apps/story/templates/layout"
      template()
    regions:
      listRegion: "#list-container"
      toolbarRegion: "#toolbar"
