define [
  'Marionette'
  'app'
  'apps/story/layout'
  'apps/story/toolbar'
  'apps/story/stories/controller'
  'apps/story/story/controller'
  'apps/story/editStory'
  'entities/story'
  ], (Backbone, App, StoriesLayout, Toolbar, StoryList, StoryDetails, StoryForm) ->
  'use strict'

  class Controller extends Backbone.Marionette.Controller
    initialize: (options) ->
      _.bindAll @, "_showStoryList", "_showStory", "_showStoryForm"
      @layout = new StoriesLayout
      options.mainRegion.show @layout
      @_showToolbar @layout.toolbarRegion

    showStories: ->
      fetchingStories = App.Entities.reqres.request "story:entities"
      $.when(fetchingStories)
        .then @_showStoryList
      Backbone.history.navigate "#stories"

    newStory: ->
      story = new App.Entities.Story
      @_showStoryForm story
      Backbone.history.navigate "#stories/new"

    showStoryById: (id) ->
      fetchingStory = App.Entities.reqres.request "story:entity", id
      $.when(fetchingStory).then @_showStory

    editStoryById: (id) ->
      fetchingStory = App.Entities.reqres.request "story:entity", id
      $.when(fetchingStory).then @_showStoryForm

    _showStoryList: (storyList) ->
      storyList = new StoryList
        region: @layout.listRegion
        stories: storyList
      storyList.showStories()

    _showStoryForm: (story) ->
      storyForm = new StoryForm
        region: @layout.listRegion
        story: story

      storyForm.on "form:submit", (data) =>
        xhr = story.save(data)
        if xhr then xhr.done => @showStories()

      storyForm.show()

    _showStory: (story) ->
      storyDetails = new StoryDetails
        region: @layout.listRegion
        story: story
      storyDetails.show()

    _showToolbar: (region) ->
      toolbar = new Toolbar { region: region }
      toolbar.show()

      toolbar.on "story:new", =>
        @newStory()

      toolbar.on "story:list", =>
        @showStories()
