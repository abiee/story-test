define (require) ->
  'use strict'

  Backbone = require "Marionette"

  class StoryView extends Backbone.Marionette.ItemView
    initialize: ->
      @model.on "invalid", (event) =>
        (@$el.find ".error").remove()
        (@$el.find ".has-error").removeClass "has-error"
        _.each event.validationError, (message, field) =>
          $field = (@$el.find "#" + field)
          $field.after $("<span>").addClass("error").html message
          $field.closest(".form-group").addClass "has-error"

    template: (serializedModel) ->
      template = require "hbs!apps/story/templates/editStory"
      template serializedModel

    events:
      "click button[type='submit']": "submitForm"

    submitForm: (event) ->
      event.preventDefault()
      @trigger "form:submit",
        name: (@$el.find "#name").val()

  class Controller extends Backbone.Marionette.Controller
    initialize: (options) ->
      @region = options.region
      @story = options.story

    show: ->
      storyView = new StoryView { model: @story }

      @listenTo storyView, "form:submit", @_submit

      @region.show storyView

    _submit: (data) ->
      @trigger "form:submit", data
