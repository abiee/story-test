define (require) ->
  'use strict'

  Backbone = require "Marionette"

  class Layout extends Backbone.Marionette.Layout
    template: ->
      template = require "hbs!apps/story/templates/storyListLayout"
      template()
    regions:
      listRegion: "#story-list-container"
      previewRegion: "#story-list-preview"
