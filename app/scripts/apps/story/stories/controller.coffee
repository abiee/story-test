define ['Marionette', './layout', './stories', './preview'], (Backbone, Layout, StoryListView, StoryPreview) ->
  'use strict'

  class Controller extends Backbone.Marionette.Controller
    initialize: (options) ->
      @region = options.region
      @stories = options.stories

    showStories: ->
      listView = new StoryListView { collection: @stories }
      @layout = new Layout

      @listenTo listView, "itemview:story:selected", @_storySelected

      @region.show @layout
      @layout.listRegion.show listView

    _showPreview: (story) ->
      @layout.previewRegion.show new StoryPreview { model: story }

    _storySelected: (view) ->
      story = view.model
      @_showPreview story
      Backbone.history.navigate "#stories/" + story.id

