define (require) ->
  'use strict'

  Backbone = require "Marionette"

  class StoryView extends Backbone.Marionette.ItemView
    tagName: "li"
    template: (serializedModel) ->
      template = require "hbs!apps/story/templates/storyItem"
      template(serializedModel)

    events:
      "click a": "selectStory"

    selectStory: (event) ->
      event.preventDefault()
      @trigger "story:selected", @model

  class StoryListView extends Backbone.Marionette.CollectionView
    itemView: StoryView
    tagName: "ul"
    id: "story-list"
    className: "story-list"
