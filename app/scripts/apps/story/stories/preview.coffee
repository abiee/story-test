define (require) ->
  'use strict'

  Backbone = require "Marionette"

  class StoryPreview extends Backbone.Marionette.ItemView
    template: (serializedModel) ->
      template = require "hbs!apps/story/templates/storyPreview"
      template serializedModel
