define (require) ->
  'use strict'

  Routefilter = require 'routefilter'
  Backbone = require 'Marionette'
  App = require 'app'

  class StoryRouter extends Backbone.Marionette.AppRouter
    before: ->
      if not @app
        StoriesApp = require("./../apps/story/app")
        @app = new StoriesApp { mainRegion: App.appRegion }

    routes:
      "stories": "listStories"
      "stories/new": "newStory"
      "stories/:id": "showStoryById"
      "stories/:id/edit": "editStoryById"

    listStories: ->
      @app.showStories()

    newStory: ->
      @app.newStory()

    showStoryById: (id) ->
      @app.showStoryById id

    editStoryById: (id) ->
      @app.editStoryById id

  new StoryRouter
