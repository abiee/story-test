define ['Marionette'], (Backbone) ->
  'use strict'

  App = new Backbone.Marionette.Application
  App.Entities = { reqres: new Backbone.Wreqr.RequestResponse }
  App.addRegions { appRegion: "#app" }

  # Start history after all app has been initialized
  App.on "initialize:after", (options) ->
    if Backbone.history then Backbone.history.start()

  App
