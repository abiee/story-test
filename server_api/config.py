# -*- config:utf-8 -*-

import logging
from datetime import timedelta

project_name = "concept"


class Config(object):
    # use DEBUG mode?
    DEBUG = False

    # use TESTING mode?
    TESTING = False

    # use server x-sendfile?
    USE_X_SENDFILE = False

    # DATABASE CONFIGURATION
    MONGO_HOST = "localhost"
    MONGO_DBNAME = project_name

    CSRF_ENABLED = True
    SECRET_KEY = "secret"  # import os; os.urandom(24)

    # LOGGING
    LOGGER_NAME = "%s_log" % project_name
    LOG_FILENAME = "%s.log" % project_name
    LOG_LEVEL = logging.INFO
    LOG_FORMAT = "%(asctime)s %(levelname)s\t: %(message)s" # used by logging.Formatter

    PERMANENT_SESSION_LIFETIME = timedelta(days=1)

    # EMAIL CONFIGURATION
    MAIL_SERVER = "localhost"
    MAIL_PORT = 25
    MAIL_USE_TLS = False
    MAIL_USE_SSL = False
    MAIL_DEBUG = False
    MAIL_USERNAME = None
    MAIL_PASSWORD = None
    DEFAULT_MAIL_SENDER = "example@%s.com" % project_name

    # SERVER
    DOMAIN_NAME = "localhost"
    API_SERVER_PROTOCOL = "http"
    API_SERVER_PORT = 5000
    APP_SERVER_PROTOCOL = "http"
    APP_SERVER_PORT = 9000
    API_SERVER_ROOT = "%s://%s:%s" % (API_SERVER_PROTOCOL, DOMAIN_NAME, API_SERVER_PORT)
    APP_SERVER_ROOT = "%s://%s:%s" % (APP_SERVER_PROTOCOL, DOMAIN_NAME, APP_SERVER_PORT)

    # see example/ for reference
    # ex: BLUEPRINTS = ['blog.app']  # where app is a Blueprint instance
    # ex: BLUEPRINTS = [('blog.app', {'url_prefix': '/myblog'})]  # where app is a Blueprint instance
    BLUEPRINTS = []
    RESOURCES = [
            ('resources.Stories', '/api/v1/stories', {'endpoint':"stories"}),
            ('resources.Story', '/api/stories/<id>', {'endpoint':"story"})
        ]


class Dev(Config):
    DEBUG = True
    MAIL_DEBUG = True
    SQLALCHEMY_ECHO = True


class Testing(Config):
    TESTING = True
    CSRF_ENABLED = False
    MONGO_HOST = "localhost"
    MONGO_DBNAME = "%s_test" % project_name
