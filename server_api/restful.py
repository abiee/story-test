# -*- coding: utf-8 -*-

import json
import datetime
from flask import make_response
from bson.objectid import ObjectId
from flask.ext import restful

api = restful.Api(catch_all_404s=True)

class APIEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return date_to_str(obj)
        elif isinstance(obj, ObjectId):
            return str(obj)
        return json.JSONEncoder.default(self, obj)

@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(json.dumps(data, cls=APIEncoder), code)
    resp.headers.extend(headers or {})
    return resp
