# -*- coding:utf-8 -*-

from flask.ext import script

import commands
from flask.ext.script import Command

if __name__ == "__main__":
    from main import app_factory
    import config

    manager = script.Manager(app_factory)
    manager.add_option("-c", "--config", dest="config", required=False, default=config.Dev)
    manager.add_command("test", commands.Test())
    manager.run()
