# -*- coding:utf-8 -*-

from flask.ext.script import Command, Option, prompt_bool

import os
import config


class CreateDB(Command):
    """Creates sqlalchemy database"""

    def run(self):
        from database import create_all

        create_all()


class DropDB(Command):
    """Drops sqlalchemy database"""

    def run(self):
        from database import drop_all

        drop_all()


class Test2(Command):
    """Run tests."""

    start_discovery_dir = "tests"

    def get_options(self):
        return [
            Option('--start_discover', '-s', dest='start_discovery',
                   help='Pattern to search for features',
                   default=self.start_discovery_dir),
        ]

    def run(self, start_discovery):
        import unittest

        if os.path.exists(start_discovery):
            argv = [config.project_name, "discover"]
            argv += ["-s", start_discovery]

            unittest.main(argv=argv)
        else:
            print("Directory '%s' was not found in project root." % start_discovery)

class Test(Command):
    "Run tests."

    start_discovery_dir = "tests"

    def get_options(self):
        return [
            Option('--start_discover', '-s', dest='start_discovery',
                help='Pattern to search for features',
                default=self.start_discovery_dir),
            Option('--nosetest-options', '-o', dest='nose_options',
                help='Options passed to nosetest')
        ]

    def run(self, start_discovery, nose_options):
        import nose

        if os.path.exists(start_discovery):
            argv = ["d", "--verbosity=2"]
            if nose_options:
                argv = argv + nose_options.split(' ')
            nose.run(argv=argv)
        else:
            print("Directory '%s' was not found in project root." % start_discovery)
