# -*- coding: utf-8 -*-

from unittest import TestCase

from bson import ObjectId
from bson.json_util import loads, object_hook, dumps
from flask import url_for

import config
from main import app_factory

class TestStory(TestCase):
    def setUp(self):
        print "Yay"

    def test_basic(self):
        self.assertEqual('a', 'a')

    def test_bad(self):
        self.assertEqual('b', 'b')
