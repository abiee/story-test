# -*- coding: utf-8 -*-

from flask import current_app as app
from flask import request
from flask.ext.restful import Resource, url_for
from flask.ext.restful import reqparse, abort, Api, Resource
from flask.ext.restful import fields, marshal, marshal_with, Api
from database import mongo

from restful import api
from resources.story import Story

stories = [{
        "id": 1,
        "name": 'First story',
        "description": 'It\'s just a test',
        "points": 3
    }, {
        "id": 2,
        "name": 'Second story',
        "description": 'Hi!',
        "points": 1
    }]



class Stories(Resource):
    def get(self):
        resource_url = api.url_for(Stories)
        url_next = "%s%s?page=2" % (app.config['API_SERVER_ROOT'], resource_url)
        url_last = "%s%s?page=5" % (app.config['API_SERVER_ROOT'], resource_url)
        items = []

        for story in stories:
            item = story
            item['links'] = [{
                    'url': "%s%s" % (app.config['API_SERVER_ROOT'],
                                    api.url_for(Story, id=story['id'])),
                    'htmlUrl': "%s/#stories/%s" % (app.config['APP_SERVER_ROOT'], story['id'])
                }]
            items.append(item)

        return {
                "items": items,
                "links": {
                        "next": url_next,
                        "last": url_last
                    }
            }, 200


    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, location="json", required=True, help="We need a name")
        parser.add_argument('description', type=str, location="json", help="We need a description")
        parser.add_argument('points', type=int, location="json", help="Ponits should be a valid integer")

        story_data = parser.parse_args()
        story_id = mongo.db.stories.insert(story_data)
        resource_url = api.url_for(Story, id=str(story_id))
        story = mongo.db.stories.find_one({"_id": story_id})

        resource_location = "%s%s" % (app.config['API_SERVER_ROOT'], resource_url)
        return story, 201, { "Location": resource_location }
