# -*- coding: utf-8 -*-

from flask import current_app as app
from flask.ext.restful import Resource, fields, marshal_with
from restful import api

stories = [{
        "id": 1,
        "name": 'First story',
        "description": 'It\'s just a test',
        "points": 3
    }, {
        "id": 2,
        "name": 'Second story',
        "description": 'Hi!',
        "points": 1
    }]


story_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'url': fields.Url('story')
}

class Story(Resource):
    @marshal_with(story_fields)
    def get(self, id):
        return {
                "id": id,
                "name": "xxxx"
            }, 200
